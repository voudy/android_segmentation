package tech.spirin.android_segmentation

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Message
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import org.tensorflow.contrib.android.TensorFlowInferenceInterface
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {

    private val REQUEST_TAKE_PHOTO = 1
    private lateinit var imageView: ImageView
    private lateinit var textView: TextView
    private lateinit var takePhotoBtn: Button
    private var mCurrentPhotoPath = ""
    private lateinit var photoURI: Uri
    private lateinit var outputImage: Bitmap
    private var workTime: Long = 0
    private lateinit var tfInterface: TensorFlowInferenceInterface
    private lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView = findViewById(R.id.imageView)
        textView = findViewById(R.id.textView)
        takePhotoBtn = findViewById(R.id.button)

        tfInterface = TensorFlowInferenceInterface(assets, "graph_dailystudio.pb")
        handler = Handler {
            imageView.setImageBitmap(outputImage)
            textView.text = workTime.toString()
            true
        }

        takePhotoBtn.setOnClickListener {
            dispatchTakePictureIntent()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            thread {
                val image: Bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, photoURI)

                // convert bitmap to array with shape [513, 513, 3]
                val imageResize = Bitmap.createScaledBitmap(image,  513, 513, false)
                val imageArray = IntArray(513 * 513)
                imageResize.getPixels(imageArray, 0, 513, 0, 0, 513, 513)
                val imageNormalizeArray = ByteArray(513 * 513 * 3)
                for (i in imageArray.indices) {
                    val curVal = imageArray[i]
                    imageNormalizeArray[i * 3 + 0] = (curVal shr 16 and 0xFF).toByte()
                    imageNormalizeArray[i * 3 + 1] = (curVal shr 8 and 0xFF).toByte()
                    imageNormalizeArray[i * 3 + 2] = (curVal and 0xFF).toByte()
                }

                val start = System.currentTimeMillis()
                tfInterface.feed("ImageTensor", imageNormalizeArray, 1, 513, 513, 3)
                tfInterface.run(arrayOf("SemanticPredictions"))
                val outputImageArray = IntArray(513 * 513)
                tfInterface.fetch("SemanticPredictions", outputImageArray)
                val finish = System.currentTimeMillis()
                workTime = finish - start + 1

                outputImage = Bitmap.createBitmap(513, 513, Bitmap.Config.ARGB_8888)
                for (i in 0..512) {
                    for (j in 0..512) {
                        if (outputImageArray[i + 513 * j] != 15) {
                            outputImage.setPixel(i, j, imageResize.getPixel(i, j))
                        } else {
                            val trueColor = imageResize.getPixel(i, j)
                            val red = (Color.red(trueColor) * 0.7 + Color.red(Color.YELLOW) * 0.3).toInt()
                            val green = (Color.green(trueColor) * 0.7 + Color.green(Color.YELLOW) * 0.3).toInt()
                            val blue = (Color.blue(trueColor) * 0.7 + Color.blue(Color.YELLOW) * 0.3).toInt()
                            outputImage.setPixel(i, j, Color.rgb(red, green, blue))
                        }
                    }
                }
                handler.sendEmptyMessage(0)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName, /* prefix */
            ".jpg", /* suffix */
            storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(
                    this,
                    "com.example.android.provider",
                    photoFile
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            }
        }
    }

}
